package com.github.grender.chat

import akka.actor.{Actor, ActorLogging, ActorRef}

trait ActorWithLogging extends Actor with ActorLogging {
  def logable(receive:Receive):Receive = new Receive {
    override def isDefinedAt(x: Any): Boolean = receive.isDefinedAt(x)

    override def apply(event: Any): Unit = {
      log.debug(s"Got message: $event")
      receive(event)
    }
  }
}

abstract class User {
  def isSame(other: User) = login == other.login && actorRef == other.actorRef

  def login:String
  def actorRef:ActorRef
}

case class LoggedUser(actorRef: ActorRef,login:String) extends User
case class AnonymousUser(actorRef: ActorRef, login:String) extends User
