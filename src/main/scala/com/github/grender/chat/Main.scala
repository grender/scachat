package com.github.grender.chat

import akka.actor.ActorSystem
import akka.event.Logging
import com.github.grender.chat.actors.{AuthManagerActor, ChannelManagerActor, ServerActor}

import scala.io.StdIn

object Main extends App {
  implicit val system = ActorSystem()
  implicit val ec = system.dispatcher
  implicit val log = Logging(system, getClass)
  val authManager = system.actorOf(AuthManagerActor.props)
  val channelManager = system.actorOf(ChannelManagerActor.props)
  val serverActor = system.actorOf(ServerActor.props(authManager,channelManager))


  StdIn.readLine()
  system.terminate()
}