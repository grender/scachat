package com.github.grender.chat.actors

import java.security.MessageDigest

import akka.actor.{ActorRef, Props}
import com.github.grender.chat.{ActorWithLogging, LoggedUser}

import scala.util.Random

object AuthManagerActor {

  val props = Props(classOf[AuthManagerActor])

  private case class UserStorage(login:String,hashedPass:String, salt:String)

  object recv {
    case class Login(login: String, password: String)

  }

  object send {
    case class LoginSuccessful(user: LoggedUser)

    case class LoginError(message: String)
  }
}

class AuthManagerActor extends ActorWithLogging{

  import AuthManagerActor._

  override def receive: Receive = {
    case recv.Login(login,password)=> {
      val authResult = auth(sender,login,password) match {
        case Left(message) => send.LoginError(message)
        case Right(user) => send.LoginSuccessful(user)
      }
      sender ! authResult
    }
  }


  private val users=collection.mutable.MutableList.empty[UserStorage]

  private def md5Password(password:String, salt:String) = MessageDigest.getInstance("MD5")
    .digest(s"$password#$salt".getBytes)
    .map(0xFF & _)
    .map("%02x".format(_))
    .mkString

  private def checkPassword(user:UserStorage,pass:String) = md5Password(pass,user.salt)==user.hashedPass

  private def createNewLoggedUser(userActorRef: ActorRef,username:String,pass:String):LoggedUser = {
    val salt = Random.nextString(20)
    val hashedPassword = md5Password(pass, salt)
    val user = UserStorage(username, hashedPassword, salt)
    users += user
    LoggedUser(userActorRef, user.login)
  }

  private def auth(userActorRef: ActorRef,login:String, pass:String):Either[String,LoggedUser]={
    users.find(_.login==login) match {
      case Some(candidate)=> {
        checkPassword(candidate,pass) match {
          case false => Left("Invalid password")
          case true => Right(LoggedUser(userActorRef, candidate.login))
        }
      }
      case None => Right(createNewLoggedUser(userActorRef,login,pass))
    }
  }
}
