package com.github.grender.chat.actors

import akka.actor.Props
import com.github.grender.chat.{ActorWithLogging, User}

import scala.collection.mutable

object ChannelManagerActor {
  type ChannelName = String

  def props = Props(classOf[ChannelManagerActor])

  object recv {

    case class JoinChannel(user: User, channel : ChannelName)

    case class GetChannelsUsers(user: User)

    case class UserLeave(user: User)

    case class UserMessage(user:User, msg: String)
  }

  object send {

    case class JoinChannelSuccess(channel: ChannelName, lastMessages: List[ChannelMessage])

    case class JoinChannelError(msg: String)

    case class ChannelsUsers(channelsUsersInfo: Map[ChannelName, List[String]])

    case class ChannelMessage(channel:ChannelName, msg: String)



  }
}

class ChannelManagerActor extends ActorWithLogging {

  import ChannelManagerActor._

  private val channelUsers = collection.mutable.Map.empty[ChannelName,collection.mutable.ListBuffer[User]]

  private val channelMessagesQueue =  collection.mutable.Map.empty[ChannelName,collection.mutable.Queue[String]]

  def sendChannelMessage(channelName:ChannelName, msg: String): Unit = {
    val messagesQueue = channelMessagesQueue.getOrElseUpdate(channelName,collection.mutable.Queue.empty[String])

    while (messagesQueue.length > 9) {
      messagesQueue.dequeue()
    }

    messagesQueue.enqueue(msg)
    channelUsers.getOrElse(channelName,List())
      .foreach(_.actorRef ! send.ChannelMessage(channelName,msg))
  }

  override def receive: Receive = logable {
    case recv.JoinChannel(user,channelName)=> {
      val usersList = channelUsers.getOrElseUpdate(channelName,mutable.ListBuffer.empty[User])
      usersList.contains(user) match {
        case true => user.actorRef ! send.JoinChannelError("You're alredy join")
        case false if usersList.size >= 10 => user.actorRef ! send.JoinChannelError("Max channel clients limit exceed")
        case false => {
          sendChannelMessage(channelName, s"${user.login} joined")
          usersList += user
          user.actorRef ! send.JoinChannelSuccess(channelName, channelMessagesQueue(channelName).map(send.ChannelMessage(channelName,_)).toList)
        }
      }
    }

    case recv.UserLeave(user)=> {
      channelUsers.foreach({
        case (channelName,users)=> {
          if(users.contains(user)) {
            users -= user
            sendChannelMessage(channelName, s"${user.login} leaved")
          }
        }
      })
    }

    case recv.UserMessage(user,text)=> {
      channelUsers.foreach({
        case (channelName,users)=> {
          if(users.contains(user)) {
            sendChannelMessage(channelName, s"${user.login}: $text")
          }
        }
      })
    }

    case UserActor.send.UserRenamed(oldUser:User,newUser:User) => {
      channelUsers.foreach({
        case (channelName,users)=> {
          if(users.contains(oldUser)) {
            users -= oldUser
            users += newUser
            sendChannelMessage(channelName, s"${oldUser.login} is now known as ${newUser.login}")
          }
        }
      })
    }

    case recv.GetChannelsUsers(user)=>{
      val usersInfo = channelUsers.filter({
        case (channelName,users)=> {
          users.contains(user)
        }
      }).mapValues(_.map(_.login).toList).toMap
      sender ! ChannelManagerActor.send.ChannelsUsers(usersInfo)
    }
  }
}
