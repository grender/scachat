package com.github.grender.chat.actors

import java.net.InetSocketAddress

import akka.actor.{ActorRef, Props}
import akka.io.{IO, Tcp}
import com.github.grender.chat.ActorWithLogging

object ServerActor {
  def props(authManager: ActorRef, channelManager: ActorRef) = Props(classOf[ServerActor],authManager, channelManager)
}

class ServerActor(authManager: ActorRef, channelManager: ActorRef) extends ActorWithLogging {
  import Tcp._
  import context.system

  IO(Tcp) ! Bind(self, new InetSocketAddress("127.0.0.1",9090))

  def receive = logable {
    case Bound(localAddress) => {
      log.info(s"Server started on ${localAddress.toString}")
    }

    case CommandFailed(b: Bind) => {
      log.error(s"Error binding to ${b.localAddress.toString}")
      context stop self
    }

    case c @ Connected(remote, local) ⇒
      val connection = sender()
      val handler = context.actorOf(UserActor.props(connection, authManager, channelManager))
      connection ! Register(handler)
  }
}
