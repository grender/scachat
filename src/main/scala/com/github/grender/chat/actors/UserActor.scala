package com.github.grender.chat.actors

import akka.actor.{ActorRef, ActorSystem, PoisonPill, Props}
import akka.dispatch.{PriorityGenerator, UnboundedStablePriorityMailbox}
import akka.io.Tcp
import akka.io.Tcp.{PeerClosed, Write}
import akka.util.ByteString
import com.github.grender.chat._
import com.typesafe.config.Config

import scala.util.Random

object UserActor {

  def props(connection: ActorRef, authManager: ActorRef, channelManager: ActorRef) =
    Props(classOf[UserActor],connection, authManager, channelManager)
    .withDispatcher("user-dispatcher")

  val LoginRegexp = "/login ([a-zA-Z0-9]+) ([a-zA-Z0-9]+)".r
  val JoinRegexp = "/join ([a-zA-Z0-9]+)".r

  object send {

    case class UserRenamed(oldUser:User, newUser:User)

  }

  class MailBox(settings: ActorSystem.Settings, config: Config) extends UnboundedStablePriorityMailbox(
    PriorityGenerator {
      case e:AuthManagerActor.send.LoginError => 0
      case e:AuthManagerActor.send.LoginSuccessful => 0
      case e:ChannelManagerActor.send.JoinChannelSuccess => 0
      case e:ChannelManagerActor.send.JoinChannelError => 0
      case e:ChannelManagerActor.send.ChannelsUsers => 0
      case PoisonPill => 100
      case _=> 50
    }
  )

}

class UserActor(connection: ActorRef, authManager: ActorRef, channelManager: ActorRef)
  extends ActorWithLogging {

  import UserActor._

  implicit val ec = context.dispatcher
  implicit val system = context.system

  def sendToClient(text: String) {
    connection ! Write(ByteString(s"$text\n"))
  }

  var user: User = AnonymousUser(self, s"Anonymous ${Random.nextInt()}")

  sendToClient(s"You nick name now: ${user.login}")

  def awaitLoginResult:Receive = logable {
    case AuthManagerActor.send.LoginError(error) => {
      sendToClient(s"Login error: $error")
      context.become(receive)
    }
    case AuthManagerActor.send.LoginSuccessful(authUser)=> {
      val oldUser = user
      user = authUser
      channelManager ! send.UserRenamed(oldUser, user)
      sendToClient(s"Success! Hello ${user.login}!")
      context.become(receive)
    }
    case Tcp.Received(_) => sendToClient(s"Error: can't process input cuz awaiting login command result")
  }

  def login(username: String, password: String): Unit = {
    log.info(s"Call login $username")

    user match {
      case LoggedUser(_, _) => sendToClient("Error: You are alredy logged in")
      case AnonymousUser(_, _) => {
        authManager ! AuthManagerActor.recv.Login(username,password)
        context.become(awaitLoginResult)
      }
    }
  }


  def awaitJoinChannel: Receive = {
    case ChannelManagerActor.send.JoinChannelSuccess(channel, lastMessages)=> {
      sendToClient(lastMessages.map(_.msg).mkString("\n"))
      context.become(receive)
    }
    case ChannelManagerActor.send.JoinChannelError(msg)=> {
      sendToClient(s"Error join channel: $msg")
      context.become(receive)
    }
    case Tcp.Received(_) => sendToClient(s"Error: can't process input cuz awaiting join command result")
  }

  def joinChannel(channelName: String) {
    log.info(s"Call join($channelName)")
    channelManager ! ChannelManagerActor.recv.JoinChannel(user,channelName)
    context.become(awaitJoinChannel)
  }

  def awaitPrintChannelsUsersResult:Receive = {
    case ChannelManagerActor.send.ChannelsUsers(usersInfo) => {
      val formattedUserList = usersInfo
        .mapValues(usersLogin=> usersLogin.map(" - " + _).mkString("\n"))
        .map( {
          case (channelName, users) => channelName+"\n" + users
        })
        .mkString("\n")
      sendToClient(s"Users list:\n$formattedUserList")
      context.become(receive)
    }
    case Tcp.Received(_) => sendToClient(s"Error: can't process input cuz awaiting login result")
  }

  def printChannelsUsers {
    log.info("Call users")
    channelManager ! ChannelManagerActor.recv.GetChannelsUsers(user)
    context.become(awaitPrintChannelsUsersResult)
  }

  def disconnect() {
    log.info("Call disconnect")
    self ! PoisonPill
    channelManager ! ChannelManagerActor.recv.UserLeave(user)
  }

  def runCommand(command: String) = command match {
    case LoginRegexp(username, pass) => login(username, pass)
    case JoinRegexp(room) => joinChannel(room)
    case "/users" => printChannelsUsers
    case "/leave" => {
      disconnect
      sendToClient("Good bye!")
    }
    case unknownCommand => sendToClient(s"Error: '$unknownCommand' is unknown command")
  }

  def receive = logable {
    case Tcp.Received(data) => {
      val userMessage = data.utf8String.replace("\n", "").replace("\r", "") //TODO:
      userMessage.startsWith("/") match {
        case true => runCommand(userMessage)
        case false => channelManager ! ChannelManagerActor.recv.UserMessage(user, userMessage)
      }
    }

    case ChannelManagerActor.send.ChannelMessage(channel, msg) => sendToClient(s"[$channel] $msg")
    case PeerClosed => disconnect
  }
}
