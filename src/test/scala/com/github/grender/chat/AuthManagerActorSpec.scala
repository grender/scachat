package com.github.grender.chat

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import com.github.grender.chat.actors.AuthManagerActor
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.duration._

class AuthManagerActorSpec extends TestKit(ActorSystem("MySpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "AuthManager" must {
    val authManager = system.actorOf(AuthManagerActor.props)

    def login(login:String,password:String) = {
      authManager ! AuthManagerActor.recv.Login(login,password)
      receiveOne(1 seconds)
    }

    "auth new user and login as same user" in {
      val u1=login("u1","p1") match {
        case AuthManagerActor.send.LoginSuccessful(user@LoggedUser(_,"u1"))=> user
        case msg=> fail(s"Got $msg")
      }

      login("u1","p1") match {
        case AuthManagerActor.send.LoginSuccessful(u2) if u1==u2 =>
        case msg=> fail(s"Got $msg")
      }
    }

    "auth other user and check login first and second" in {
      val u1=login("u1","p1") match {
        case AuthManagerActor.send.LoginSuccessful(user@LoggedUser(_,"u1"))=> user
        case msg=> fail(s"Got $msg")
      }

      val u2=login("u2","p2") match {
        case AuthManagerActor.send.LoginSuccessful(user@LoggedUser(_,"u2"))=> user
        case msg=> fail(s"Got $msg")
      }

      assert(u1!=u2)

      login("u2","p2") match {
        case AuthManagerActor.send.LoginSuccessful(u2_2) if u2==u2_2=>
        case msg=> fail(s"Got $msg")
      }
    }

    "got error on invalid password" in {
      val u1=login("u1","p0") match {
        case AuthManagerActor.send.LoginError("Invalid password")=>
        case msg=> fail(s"Got $msg")
      }
    }


  }
}
