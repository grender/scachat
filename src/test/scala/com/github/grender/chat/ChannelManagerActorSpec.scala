package com.github.grender.chat

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.github.grender.chat.actors.{ChannelManagerActor, UserActor}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.collection.Map

class ChannelManagerActorSpec extends TestKit(ActorSystem("MySpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  import ChannelManagerActor._
  
  def createUser(probe: TestProbe, loginA: String) = new User {
    override def actorRef: ActorRef = probe.testActor

    override def login: String = loginA
  }

  val userActorProbe1 = TestProbe()
  val userActorProbe2 = TestProbe()
  val userActorProbe3 = TestProbe()

  val user1 = createUser(userActorProbe1, "user1")
  val user2 = createUser(userActorProbe2, "user2")
  val user2newLogin = createUser(userActorProbe2, "user2_new_login")
  val user3 = createUser(userActorProbe3, "user3")


  val channelManager = system.actorOf(props)

  "ChannelManagerActor" must {
    "successful join" in {
      channelManager ! recv.JoinChannel(user1, "A")
      channelManager ! recv.JoinChannel(user2, "A")
      channelManager ! recv.JoinChannel(user2, "B")
      channelManager ! recv.JoinChannel(user3, "B")

      userActorProbe1.expectMsgPF() {
        case send.JoinChannelSuccess("A", _) =>
      }
      userActorProbe2.expectMsgPF() {
        case send.JoinChannelSuccess("A", _) =>
      }
      userActorProbe1.expectMsgPF() {
        case send.ChannelMessage("A", "user2 joined") =>
      }


      userActorProbe2.expectMsgPF()({
        case send.JoinChannelSuccess("B", _) =>
      })
      userActorProbe3.expectMsgPF()({
        case send.JoinChannelSuccess("B", _) =>
      })
      userActorProbe2.expectMsgPF() {
        case send.ChannelMessage("B", "user3 joined") =>
      }
    }

    "got error on second join" in {
      channelManager ! recv.JoinChannel(user1, "A")
      userActorProbe1.expectMsgPF()({
        case send.JoinChannelError("You're alredy join") =>
      })
    }

    "send messages to channel" in {
      channelManager ! recv.UserMessage(user1, "TestMsg 1")
      userActorProbe1.expectMsg(send.ChannelMessage("A", "user1: TestMsg 1"))
      userActorProbe2.expectMsg(send.ChannelMessage("A", "user1: TestMsg 1"))
      userActorProbe3.expectNoMessage()

      channelManager ! recv.UserMessage(user2, "TestMsg 2")
      userActorProbe1.expectMsg(send.ChannelMessage("A", "user2: TestMsg 2"))
      userActorProbe2.expectMsg(send.ChannelMessage("A", "user2: TestMsg 2"))
      userActorProbe2.expectMsg(send.ChannelMessage("B", "user2: TestMsg 2")) // TODO: author got 2 messages from both channels
      userActorProbe3.expectMsg(send.ChannelMessage("B", "user2: TestMsg 2"))

      channelManager ! recv.UserMessage(user3, "TestMsg 3")
      userActorProbe1.expectNoMessage()
      userActorProbe2.expectMsg(send.ChannelMessage("B", "user3: TestMsg 3"))
      userActorProbe3.expectMsg(send.ChannelMessage("B", "user3: TestMsg 3"))
    }

    "get correct info about channels users" in {
      channelManager.tell(recv.GetChannelsUsers(user1), user1.actorRef)
      userActorProbe1.expectMsgPF() {
        case send.ChannelsUsers(usersMap) if usersMap == Map("A" -> List("user1", "user2")) =>
      }

      channelManager.tell(recv.GetChannelsUsers(user2), user2.actorRef)
      userActorProbe2.expectMsgPF() {
        case send.ChannelsUsers(usersMap) if usersMap == Map("A" -> List("user1", "user2"), "B" -> List("user2", "user3")) =>
      }

      channelManager.tell(recv.GetChannelsUsers(user3), user3.actorRef)
      userActorProbe3.expectMsgPF() {
        case send.ChannelsUsers(usersMap) if usersMap == Map("B" -> List("user2", "user3")) =>
      }
    }

    "correct react on leave user" in {
      channelManager ! recv.UserLeave(user1)
      userActorProbe2.expectMsg(send.ChannelMessage("A", "user1 leaved"))

      channelManager.tell(recv.GetChannelsUsers(user2), user2.actorRef)
      userActorProbe2.expectMsgPF() {
        case send.ChannelsUsers(usersMap) if usersMap == Map("A" -> List("user2"), "B" -> List("user2", "user3")) =>
      }

      channelManager ! recv.UserMessage(user1, "TestMsg 1")
      userActorProbe1.expectNoMessage()
      userActorProbe2.expectNoMessage()
      userActorProbe3.expectNoMessage()
    }

    "correct react on user rename" in {
      channelManager ! UserActor.send.UserRenamed(user2, user2newLogin)
      userActorProbe1.expectNoMessage()
      userActorProbe2.expectMsg(send.ChannelMessage("A", "user2 is now known as user2_new_login"))
      userActorProbe2.expectMsg(send.ChannelMessage("B", "user2 is now known as user2_new_login")) // TODO: user got duplicate
      userActorProbe3.expectMsg(send.ChannelMessage("B", "user2 is now known as user2_new_login"))
    }

    "correct send last 10 messages on join" in {
      for(i<- 1 to 10) {
        channelManager ! recv.UserMessage(user2newLogin, i.toString)
      }

      val correctMessagesList =
        (1 to 10).map(msgNum=>s"${user2newLogin.login}: $msgNum")
        .toList
        .map(send.ChannelMessage("B",_))
        .tail :::List(send.ChannelMessage("B","user1 joined"))

      channelManager ! recv.JoinChannel(user1, "B")
      userActorProbe1.expectMsg(send.JoinChannelSuccess("B",correctMessagesList))
    }

    "limit 10 clients to channel" in {
      val probeAndUsers = (1 to 10).map(num => {
        val probe= TestProbe()
        (probe,createUser(probe, s"limit_test_user_$num"))
      })
      probeAndUsers.foreach({
        case (probe,user)=> {
          channelManager ! recv.JoinChannel(user, "LIMIT_TEST")
          probe.expectMsgPF() {
            case send.JoinChannelSuccess("LIMIT_TEST", _)=>
          }
        }
      })


      channelManager ! recv.JoinChannel(user1, "LIMIT_TEST")
      userActorProbe1.expectMsg(send.JoinChannelError("Max channel clients limit exceed"))

    }
  }
}