package com.github.grender.chat

import akka.actor.ActorSystem
import akka.io.Tcp
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import akka.util.ByteString
import com.github.grender.chat.actors.{AuthManagerActor, ChannelManagerActor, UserActor}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class UserActorSpec extends TestKit(ActorSystem("MySpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  val connectionProbe = TestProbe()
  val authManagerProbe = TestProbe()
  val channelManagerProbe = TestProbe()

  implicit def probeExtension(t:TestProbe) = new {

    def expectTcpWithText(text: String) =
    t.expectMsgPF() {
      case Tcp.Write(bs, Tcp.NoAck) if bs.utf8String == text =>
    }
  }


  "UserActor" must {
    val userActor = system.actorOf(UserActor.props(connectionProbe.testActor,authManagerProbe.testActor, channelManagerProbe.testActor))
    var currentUserLogin = ""

    def testDifferentLoginBehavior(userStatus:String,currentUser: =>User) = {
      s"correct send messages with $userStatus user" in {
        userActor ! Tcp.Received(ByteString("Test message\n"))
        channelManagerProbe.expectMsgPF() {
          case ChannelManagerActor.recv.UserMessage(user, "Test message") if user.isSame(currentUser) =>
        }
      }

      s"correct join channel and await result with $userStatus user" in {
        userActor ! Tcp.Received(ByteString("/join A\n"))
        channelManagerProbe.expectMsgPF() {
          case ChannelManagerActor.recv.JoinChannel(user,"A") if user.isSame(currentUser) =>
        }

        userActor ! Tcp.Received(ByteString("random text\n"))
        connectionProbe.expectTcpWithText("Error: can't process input cuz awaiting join command result\n")

        userActor ! ChannelManagerActor.send.JoinChannelSuccess("A",List(
          ChannelManagerActor.send.ChannelMessage("A","msg1"),
          ChannelManagerActor.send.ChannelMessage("A","msg2")
        ))
        connectionProbe.expectTcpWithText("msg1\nmsg2\n")
      }

      s"correct react join error with $userStatus user" in {
        userActor ! Tcp.Received(ByteString("/join A\n"))
        channelManagerProbe.expectMsgPF() {
          case ChannelManagerActor.recv.JoinChannel(user,"A") if user.isSame(currentUser) =>
        }

        userActor ! ChannelManagerActor.send.JoinChannelError("join_error")
        connectionProbe.expectTcpWithText("Error join channel: join_error\n")
      }

      s"correct get user lists with $userStatus user" in {
        userActor ! Tcp.Received(ByteString("/users\n"))

        channelManagerProbe.expectMsgPF() {
          case ChannelManagerActor.recv.GetChannelsUsers(user) if user.isSame(currentUser) =>
        }

        userActor ! ChannelManagerActor.send.ChannelsUsers(Map(
          "A"->List("u1","u2"),
          "B"->List("u2","u3")
        ))

        connectionProbe.expectTcpWithText(
          """Users list:
            |A
            | - u1
            | - u2
            |B
            | - u2
            | - u3
            |""".stripMargin)
      }

    }

    "get user welcome message" in {
      val nickNameRegexp = "You nick name now: (.*)\n".r
      connectionProbe.expectMsgPF() {
        case Tcp.Write(bs, Tcp.NoAck) if bs.utf8String.startsWith("You nick name now: ") => {
          bs.utf8String match {
            case nickNameRegexp(name)=> {
              currentUserLogin = name
            }
            case _=> fail("Can't parse welcome message to get regexp")
          }
        }
      }
    }

    testDifferentLoginBehavior("anonymous",AnonymousUser(userActor,currentUserLogin))

    "correct react on login error" in {
      userActor ! Tcp.Received(ByteString("/login user invalidPass\n"))
      authManagerProbe.expectMsg(AuthManagerActor.recv.Login("user","invalidPass"))

      userActor ! AuthManagerActor.send.LoginError("login_error")
      connectionProbe.expectTcpWithText(s"Login error: login_error\n")

    }

    "correct login and await result" in {
      userActor ! Tcp.Received(ByteString("/login user pass\n"))
      authManagerProbe.expectMsg(AuthManagerActor.recv.Login("user","pass"))

      userActor ! Tcp.Received(ByteString("random text\n"))
      connectionProbe.expectTcpWithText("Error: can't process input cuz awaiting login command result\n")

      val loggedUser = LoggedUser(userActor,"user")
      userActor ! AuthManagerActor.send.LoginSuccessful(loggedUser)
      channelManagerProbe.expectMsgPF() {
        case UserActor.send.UserRenamed(oldUser, user) if user == loggedUser && oldUser.login == currentUserLogin =>
      }
      currentUserLogin = loggedUser.login
      connectionProbe.expectTcpWithText(s"Success! Hello $currentUserLogin!\n")
    }

    "can't login second time" in {
      userActor ! Tcp.Received(ByteString("/login user pass\n"))
      connectionProbe.expectTcpWithText("Error: You are alredy logged in\n")
    }

    testDifferentLoginBehavior("logged",LoggedUser(userActor,currentUserLogin))

    "correct run /leave command" in {
      val pillProbe = TestProbe()
      pillProbe.watch(userActor)
      userActor ! Tcp.Received(ByteString("/leave\n"))

      channelManagerProbe.expectMsgPF() {
        case ChannelManagerActor.recv.UserLeave(user) if user.login==currentUserLogin =>
      }

      connectionProbe.expectTcpWithText("Good bye!\n")

      pillProbe.expectTerminated(userActor)
    }

    "correct terminate on close connection" in {
      val connectionProbe2 = TestProbe()
      val authManagerProbe2 = TestProbe()
      val channelManagerProbe2 = TestProbe()
      val userActor2 = system.actorOf(UserActor.props(connectionProbe2.testActor,authManagerProbe2.testActor, channelManagerProbe2.testActor))
      connectionProbe2.expectMsgPF() {
        case Tcp.Write(bs,Tcp.NoAck) => // skip welcome message
      }
      val disconnectProbe = TestProbe()
      disconnectProbe.watch(userActor2)
      userActor2 ! Tcp.PeerClosed
      connectionProbe2.expectNoMessage()
      disconnectProbe.expectTerminated(userActor2)
    }
  }
}